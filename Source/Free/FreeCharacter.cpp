// Copyright Epic Games, Inc. All Rights Reserved.

#include "FreeCharacter.h"
#include "FreeProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "ClasesDT.h"
#include "GameFramework/InputSettings.h"


//////////////////////////////////////////////////////////////////////////
// AFreeCharacter

AFreeCharacter::AFreeCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	TurnRateGamepad = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

}

void AFreeCharacter::GetInitialStats()
{

	UE_LOG(LogTemp, Warning, TEXT("Get initial stats"));
	if(ClassDB && !miClass && !className.IsNone()){
		UE_LOG(LogTemp, Warning, TEXT("entra al if"));
		static const FString context = FString("Obtener estadisticas iniciales");

		FName classId = "0";
		if (className == "INGENIERO")
		{
			classId = "1";
		}else
		{
			classId = "2";
		}
		FClasesDT* clase = ClassDB->FindRow<FClasesDT>(classId, context, false);
		if(clase)
		{
			UE_LOG(LogTemp, Warning, TEXT("entra al clase"));
			miClass = clase;
			level = 1;
			exp = 0;
			neededExp = 5;
			maxHp = clase->initialHp;
			currentHp = maxHp;
			damage = clase->damageModifier;
			skillName = clase->skill1;
			rifle = clase->hasRifle;
			
		}
	}else {
		if(miClass) {
			UE_LOG(LogTemp, Warning, TEXT("%s"), *UEnum::GetValueAsString(miClass->myclass.GetValue()))
		}
	}
	
}

void AFreeCharacter::GetExp()
{

	exp++;

	if (exp >= neededExp)
	{

		LevelUp();
		
	}
	
}

void AFreeCharacter::LevelUp()
{

	level++;
	exp = 0;
	neededExp += 2;
	HealthUp();
	AtkUp();
	
}

void AFreeCharacter::HealthUp()
{

	maxHp += 10;
	currentHp += 10;
	OnDamaged.Broadcast(maxHp, currentHp);
	
}

void AFreeCharacter::AtkUp()
{

	damage += 0.1f;
	
}

void AFreeCharacter::JumpUp()
{

	JumpMaxCount++;
	
}

void AFreeCharacter::SpeedUp()
{

	moveSpeed += .1f;
	
}

void AFreeCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	isShooting = false;

}

void AFreeCharacter::Tick(float DeltaTime)
{

	Super::Tick(DeltaTime);

	if (bInputPressed)
	{

		FollowTime += DeltaTime;
		
	}
	
}

//////////////////////////////////////////////////////////////////////////// Input

void AFreeCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// Bind fire event
	PlayerInputComponent->BindAction("PrimaryAction", IE_Pressed, this, &AFreeCharacter::OnPrimaryAction);
	PlayerInputComponent->BindAction("PrimaryAction", IE_Released, this, &AFreeCharacter::OnPrimaryActionRelease);

	PlayerInputComponent->BindAction("UseAbility", IE_Pressed, this, &AFreeCharacter::OnAbility);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("Move Forward / Backward", this, &AFreeCharacter::MoveForward);
	PlayerInputComponent->BindAxis("Move Right / Left", this, &AFreeCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "Mouse" versions handle devices that provide an absolute delta, such as a mouse.
	// "Gamepad" versions are for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn Right / Left Mouse", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Look Up / Down Mouse", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn Right / Left Gamepad", this, &AFreeCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("Look Up / Down Gamepad", this, &AFreeCharacter::LookUpAtRate);
}

void AFreeCharacter::OnPrimaryAction()
{

	bInputPressed = true;
	FollowTime = 0;
	// Trigger the OnItemUsed Event
	//OnUseItem.Broadcast();
}

void AFreeCharacter::OnPrimaryActionRelease()
{

	bInputPressed = false;

	if (!isShooting)
	{

		isShooting = true;
		OnInputReleased.Broadcast();

		if(rifle)
		{

			if (FollowTime > shootMinVal)
			{

				auto q = FollowTime / shootMinVal;

				if (q > 5) q = 5;
				ShootBullets(q, false);
				FollowTime = 0.f;
		
			}else
			{

				ShootBullets(1, false);
				FollowTime = 0.f;
		
			}
			
		}else
		{
			if (FollowTime > 1.5f)
			{

				ShootBullets(1, true);
				FollowTime = 0.f;
				
			}else
			{

				ShootBullets(1, false);
				FollowTime = 0.f;
				
			}
			
		}
		
		
	}
	
}

void AFreeCharacter::ShootBullets(float bullets, bool snipe)
{

	OnUseItem.Broadcast(damage, snipe);
	bullets--;
	if (bullets > 0)
	{

		FTimerDelegate rDel = FTimerDelegate::CreateUObject(this, &AFreeCharacter::ShootBullets, bullets, snipe);
		GetWorld()->GetTimerManager().SetTimer(fHandler, rDel, shootDelayTime, false);
		
	}else
	{

		GetWorld()->GetTimerManager().SetTimer(fHandler, this, &AFreeCharacter::Reload, reloadTime, false);
		
	}
	
}

void AFreeCharacter::Reload()
{

	isShooting = false;
	
}

void AFreeCharacter::OnAbility()
{

	UE_LOG(LogTemp, Warning, TEXT("OnAbility"));

	if (skillName.IsEqual("Turret"))
	{

		OnUseAbility.Broadcast();
		
	}else
	{

		OnUseElectricRay.Broadcast();
		
	}
	
}

void AFreeCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnPrimaryAction();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AFreeCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

void AFreeCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value * moveSpeed);
	}
}

void AFreeCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value * moveSpeed);
	}
}

void AFreeCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

void AFreeCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * TurnRateGamepad * GetWorld()->GetDeltaSeconds());
}

bool AFreeCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFreeCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AFreeCharacter::EndTouch);

		return true;
	}
	
	return false;
}

void AFreeCharacter::PlayerGetDamaged(int enemyDamage)
{

	currentHp -= enemyDamage;
	OnDamaged.Broadcast(maxHp, currentHp);
	
}
