// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "ClasesDT.generated.h"

UENUM(BlueprintType)
enum EClases {
	INGENIERO,
	CAPITAN
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct FREE_API FClasesDT : public FTableRowBase
{

	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EClases> myclass;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float damageModifier;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName skill1;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool hasRifle;

	FClasesDT() : myclass(INGENIERO), initialHp(100), damageModifier(1), skill1("Turret"), hasRifle(true) {}
	
};
