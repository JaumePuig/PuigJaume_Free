// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "ClasesDT.h"
#include "Engine/DataTable.h"
#include "GameFramework/Character.h"
#include "FreeCharacter.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class USceneComponent;
class UCameraComponent;
class UAnimMontage;
class USoundBase;

// Declaration of the delegate that will be called when the Primary Action is triggered
// It is declared as dynamic so it can be accessed also in Blueprints
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUseItem, float, damage, bool, snipe);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUseAbility);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUseElectricRay);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInputReleased);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEnemyKilled);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDamaged, float, vidaMax, float, vidaActual);

UCLASS(config=Game)
class AFreeCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

public:
	AFreeCharacter();

	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite, Category=DATATABLES)
	UDataTable* ClassDB;
	UPROPERTY(BlueprintReadWrite)
	FName className;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	int level;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	int exp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	int neededExp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	int maxHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	int currentHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	float damage;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	FName skillName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	float moveSpeed = 0.1f;

	FClasesDT* miClass;

	UFUNCTION(BlueprintCallable)
	void GetInitialStats();
	UFUNCTION(BlueprintCallable)
	void GetExp();
	UFUNCTION(BlueprintCallable)
	void LevelUp();
	UFUNCTION(BlueprintCallable)
	void HealthUp();
	UFUNCTION(BlueprintCallable)
	void AtkUp();
	UFUNCTION(BlueprintCallable)
	void JumpUp();
	UFUNCTION(BlueprintCallable)
	void SpeedUp();

protected:
	virtual void BeginPlay();
	virtual void Tick(float DeltaTime) override;

public:
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float TurnRateGamepad;

	/** Delegate to whom anyone can subscribe to receive this event */
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnUseItem OnUseItem;

	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnUseAbility OnUseAbility;
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnUseElectricRay OnUseElectricRay;
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnInputReleased OnInputReleased;
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnEnemyKilled OnEnemyKilled;
	UPROPERTY(BlueprintAssignable, Category = "Interaction")
	FOnDamaged OnDamaged;
	
protected:
	
	/** Fires a projectile. */
	void OnPrimaryAction();
	void OnPrimaryActionRelease();
	void ShootBullets(float bullets, bool snipe);
	void Reload();
	void OnAbility();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles strafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	bool rifle;

	bool bInputPressed;
	float FollowTime;

	bool isShooting;
	FTimerHandle fHandler;
	float shootMinVal = 0.25f;
	float shootDelayTime = 0.15f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Player Stats")
	float reloadTime = 0.5f;

	UFUNCTION(BlueprintCallable)
	void PlayerGetDamaged(int enemyDamage);
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool GetInputPressed() { return bInputPressed; }
	UFUNCTION(BlueprintCallable)
	FORCEINLINE float GetFollowTime() { return FollowTime; }
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool GetIsShooting() { return isShooting; }
};

