// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TP_WeaponComponent.generated.h"

class AFreeCharacter;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class FREE_API UTP_WeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Projectile)
	bool PhysicProjectile = true;
	
	/** Projectile class to spawn */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Projectile)
	TSubclassOf<class AFreeProjectile> ProjectileClass;

	/** Sound to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;
	
	/** AnimMontage to play each time we fire */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UAnimMontage* FireAnimation;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector MuzzleOffset;

	// Cosas de raycast de escoputa

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector shootPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Proyectile)
	bool bUseRaycast;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Proyectile)
	float shootSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
	float shootRange = 1000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
	int projectiles = 5.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
	float minAccuracy = -100.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Proyectile)
	float maxAccuracy = 100.f;

	/** Sets default values for this component's properties */
	UTP_WeaponComponent();

	/** Attaches the actor to a FirstPersonCharacter */
	UFUNCTION(BlueprintCallable, Category="Weapon")
	void AttachWeapon(AFreeCharacter* TargetCharacter);

	/** Make the weapon Fire a Projectile */
	UFUNCTION(BlueprintCallable, Category="Weapon")
	void Fire(float damage, bool snipe);

protected:
	/** Ends gameplay for this component. */
	UFUNCTION()
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
		

private:
	/** The Character holding this weapon*/
	AFreeCharacter* Character;
};
